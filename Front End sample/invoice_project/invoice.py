from flask import Flask, render_template
import pandas as pd
import csv
app = Flask(__name__)

df = pd.read_csv('temp_record.csv')
df_temp = df.filter(['Code_Product','Brand','Product_Name','Unit','Unit_Price'],axis=1)
posts = []
for i in range(len(df_temp)):
    dic = {}
    for j in df_temp:
        dic[j] = df_temp[j].iloc[i]
    posts.append(dic)
print(posts)

@app.route('/home')
def home():
    return render_template('home.html', posts=posts)

@app.route('/invoice')

def invoice():
    return render_template('template.html')

if __name__ == '__main__':
    app.run(debug=True)