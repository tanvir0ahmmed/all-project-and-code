import scrapy
from os import path
import csv
import pandas as pd
from csv import writer
class NdtvSpider(scrapy.Spider):
    name = 'project_0'
    start_urls = [
        #'https://gadgets.ndtv.com/mobiles/smartphones?facet[brand]=Lenovo,LG,Infinix,Tecno,Nokia,Honor,Huawei,Realme,Apple,Vivo,Xiaomi,Oppo,Samsung&facet[v_d_RAM]=6000-7999,4000-5999,3000-3999,2000-2999,8000-20000&sort=product_release_date&order=desc'
        'https://gadgets.ndtv.com/mobiles/all-brands'
    ] 

    def make_csv(self,rel,img,title,value,info):
        from csv import writer
        brand = ['Lenovo','LG','Infinix','Tecno','Nokia','Honor','Huawei','Realme','Apple','Vivo','Xiaomi','Oppo','Samsung']
        my_dict = {}

        k = 1
        for i in brand:
            if i in info:
                k = 1
        if k == 1:
            my_dict['Image'] = img[0]
            try:
                my_dict['Release Status'] = rel[1]

            except:
                my_dict['Release Status'] = "NA"
                print(rel)
            try:
                my_dict['Release Date'] = rel[3]
            except:
                my_dict['Release Date'] = "NA"
                print(rel)
            for i in range(len(title)):
                try:
                    my_dict[title[i]] = value[i]  
                except:
                    print('Error with- ','\n',title,'\n',value)  
                    continue
            #for i in range(0,len(info),2):
            
            i = 0
            '''
            while(i<len(info)):
                try:
                    if info[i] in title:
                        my_dict[info[i]+' with full details'] = info[i+1]
                        i+=2
                    elif info[i] != 'SIM 1' and info[i] != 'SIM 2' and info[i] != 'SIM 3' and info[i] != 'SIM 4':
                        my_dict[info[i]] = info[i+1]
                        i+=2
                    else:
                        i+=1
                    print('Complete ', i)
                except:
                    i+=1
                    print('Error with ', len(info) )
                    continue
            '''
            a = ""
            b = ""
            c = ""
            for i in range(len(info)):
                if info[i] == 'Brand':
                    a = info[i+1]
                elif info[i] == 'Model':
                    b = info[i+1]
                elif info[i] == 'Colours':
                    c = info[i+1]
            my_dict['Brand'] = a
            my_dict['Model'] = b
            my_dict['Colours'] = c
            #print('ok')
            f = 0
            '''
            a_string = ""
            numbers = []
            try:
                a_string = rel[3]
                print(a_string)
                for word in a_string.split():
                    if word.isdigit():
                        numbers.append(int(word))
            except:
                f = 1
            try:
                if f == 1:
                    a_string = my_dict['Release Date']
                    
                    print(a_string)
                    for word in a_string.split():
                        if word.isdigit():
                            numbers.append(int(word))
                #elif my_dict['Release Status'] != 'Upcoming':
                    #numbers.append(1010)
            except:
                if my_dict['Release Status'] != 'Upcoming':
                    numbers.append(1010)
                print('What can i do ?')
            f = 0
            if my_dict['Release Status'] != 'Upcoming' and len(numbers) == 0:
                numbers.append(1010)
            for i in numbers:
                if i < 2019:
                    f = 1
            print(numbers)
            '''
            if f == 0:
                print('i am in')
                field_names = list(my_dict.keys())
                save = 'key_spec.csv'
                #print(field_names)
                if path.exists(save) == False:
                    with open(save, 'w') as csvfile: 
                        writer = csv.DictWriter(csvfile, fieldnames = field_names) 
                        writer.writeheader() 
                        writer.writerows([my_dict]) 
                #'''
                else:
                    df = pd.read_csv(save)
                    col = list(df.columns)
                    #print('col- ',col)
                    for i in field_names:
                        if (i not in col) :
                            #print(i)
                            df[i] = "NA"
                            col.append(i)
                    #print('field_names- ',field_names)
                    #print('my_dic', my_dict)
                    tmp_dic = {}
                    for i in col:
                        if (i not in field_names):
                            #print('->',i)
                            tmp_dic[i] = "NA"
                            #print('my_dic', tmp_dic)
                        else:
                            tmp_dic[i] = my_dict[i] 
                    #for i in my_dict:
                    #my_dict = {**tmp_dic, **my_dict}  
                    my_dict = tmp_dic.copy()
                    #print(my_dict) 
                    '''
                    if len(df.columns) < len(field_names):
                        for i in field_names:
                            col = list(df.columns)
                            print(col,'\n',field_names)
                            if (i not in col) and (str(i) not in col):
                                print(i)
                                df[i] = "NA"
                    '''
                    df.to_csv(save,index=False)
                    # Open file in append mode
                    with open(save, 'a+', newline='') as write_obj:
                        # Create a writer object from csv module
                        csv_writer = writer(write_obj)
                        # Add contents of list as last row in the csv file
                        csv_writer.writerow(list(my_dict.values()))
                        #print(df.head()) 
    def parse(self, response):
        get_link =  response.xpath("//div[@class='brand logo_brand row']//a[@class='rvw-title']//@href").extract()
        #response.xpath("//div[@class='_lpdscn']//h3[@class='_hd']//a//@href").extract()
        #get_model_name = response.xpath('//*[contains(concat( " ", @class, " " ), concat( " ", "_hd", " " ))]//a//@title').extract()
        #print('size of outter link ',len(get_link))
        for phn_link in get_link:
            yield scrapy.Request(phn_link, callback=self.inside_inner)
    def inside_inner(self,response):
        get_inner_link = response.xpath("//div[@class='brand row']//div[@class='rvw-imgbox']//a//@href").extract()
        get_model_name = response.xpath("//a[@class='rvw-title']//@title").extract()
        #print('size of inner link ',len(get_inner_link))
        for phn_link in get_inner_link:
            yield scrapy.Request(phn_link, callback=self.inside_brand)
    def inside_brand(self, response):
        title = response.xpath('//*[contains(concat( " ", @class, " " ), concat( " ", "_ttl", " " ))]//text()').extract()
        value = response.xpath('//*[contains(concat( " ", @class, " " ), concat( " ", "_vltxt", " " ))]//text()').extract()
        release = response.xpath("//ul[@class='_flx _dt']//li//text()").extract()
        img = response.xpath('//*[contains(concat( " ", @class, " " ), concat( " ", "__arModalBtn", " " ))]//img//@src').extract()
        #model_name = 
        info = response.xpath("//div[@class='_gry-bg _spctbl _ovfhide']//td//text()").extract()
                
                
        #price =  response.xpath("//div[@class='_trtwgt stkmrgn']//a//text()").extract()
        #print('release: ', release)
        self.make_csv(release,img,title,value,info)




'''
if i == 'Brand' and (i not in title):
                    modify_info_key[info[i]] = info[i+1]
                elif i == 'Model' and (i not in title):
                    modify_info_key[info[i]] = info[i+1]
                elif i == 'Release date' and (i not in title):
                    modify_info_key[info[i]] = info[i+1]
                elif i == 'Release date' and (i not in title):
                    modify_info_key[info[i]] = info[i+1]
                elif i == 'Weight (g)' and (i not in title):
                    modify_info_key[info[i]] = info[i+1]
                elif i == 'Fast charging' and (i not in title):
                    modify_info_key[info[i]] = info[i+1]
                elif i == 'Colours' and (i not in title):
                    modify_info_key[info[i]] = info[i+1]
                elif i == 'Screen size (inches)' and (i not in title):
                    modify_info_key[info[i]] = info[i+1]
                elif i == 'Touchscreen' and (i not in title):
                    modify_info_key[info[i]] = info[i+1]
                elif i == 'Resolution' and (i not in title):
                    modify_info_key[info[i]] = info[i+1]
                elif i == 'Rear camera':
                    modify_info_key[info[i]] = info[i+1]
                elif i == 'Rear autofocus' and (i not in title):
                    modify_info_key[info[i]] = info[i+1]
                elif i == 'Rear flash' and (i not in title):
                    modify_info_key[info[i]] = info[i+1]
                elif i == 'Front camera':
                    modify_info_key[info[i]] = info[i+1]
                elif i == 'Pop-Up Camera' and (i not in title):
                    modify_info_key[info[i]] = info[i+1]
                elif i == 'Skin' and (i not in title):
                    modify_info_key[info[i]] = info[i+1]
                elif i == 'USB Type-C' and (i not in title):
                    modify_info_key[info[i]] = info[i+1]
                elif i == 'Headphones' and (i not in title):
                    modify_info_key[info[i]] = info[i+1]
                elif i == 'Bluetooth' and (i not in title):
                    modify_info_key[info[i]] = info[i+1]
                elif i == 'GPS' and (i not in title):
                    modify_info_key[info[i]] = info[i+1]

'''