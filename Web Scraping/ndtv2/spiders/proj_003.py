import scrapy


class Proj003Spider(scrapy.Spider):
    name = 'proj_003'
    allowed_domains = ['http://gadgets.ndtv.com']
    start_urls = ['http://gadgets.ndtv.com/mobiles/alcatel-phones/']
    #start_urls = ['http://gadgets.ndtv.com/alcatel-1v-price-in-india-91104']

    def parse(self, response):
        img_src = response.xpath("//div[@class='_pdmimg __arModalBtn _flx']//img/@src").get()
        print("###########################",img_src)
        brand_phn_link = response.xpath("//div[@class='rvw-imgbox']//a/@href").getall()
        #print(brand_phn_link)

        #yield response.follow(url='https://gadgets.ndtv.com/alcatel-1v-price-in-india-91104', callback=self.ami)
        for link in brand_phn_link:
            #yield response.follow(url=link, callback=self.ami)
            yield scrapy.Request(link, callback=self.ami)

    
    def ami(self, response):
        print("#####################################################")

