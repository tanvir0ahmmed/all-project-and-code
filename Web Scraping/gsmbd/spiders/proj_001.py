import scrapy
import logging


class Proj001Spider(scrapy.Spider):
    name = 'proj_001'
    allowed_domains = ['www.gsmarena.com.bd']
    start_urls = ['https://www.gsmarena.com.bd/']

    def parse(self, response):
        home = response.xpath("//li[@class='col-md-6']").getall()
        brand = response.xpath("//li[@class='col-md-6']//a[@title]//text()").getall()
        brand_link = response.xpath("//li[@class='col-md-6']//a/@href").getall()
        #print(' ####################### Start ######################### ')

        for link, name in zip(brand_link, brand):
            #print(type(link), name)
            yield response.follow(url=link, callback=self.inside_brand)
        #print(' ####################### END ######################### ')

    def inside_brand(self, response):
        #logging.info(response.url)
        brand_phn_link = response.xpath("//div[@class='product-thumb pro_thum']//a/@href").getall()    
        #print(' ####################### Start ######################### ')
        brand_phn_link = list(set(brand_phn_link))
        #print(brand_phn_link, '\n', len(brand_phn_link))
        for phn_link in brand_phn_link:
            yield response.follow(url=phn_link, callback=self.phone_page)
        #print(' ####################### END ######################### ')

    def phone_page(self, response):
        img_src = response.xpath("//div[@class='col-md-6']//img/@src").get()
        print(' ####################### Start ######################### ')
        #net = response.xpath("//h3[@class='heading']//text()").getall()
        response = response.replace(body=response.body.replace(b'<br>', b' '))
        col = response.xpath("//th[@class='col-xs-5']//text()").extract()
        val = response.xpath("//td[@class='col-xs-7']//text()").extract()
        dic = {}
        print("Len ", len(col),'    ' ,len(val), ' ',val)
        for i in range(len(col)):
            dic[col[i]] = val[i]
        dic['image'] = img_src
        #print("Image ",img_src)
        yield dic
        print(' ####################### END ######################### ')

