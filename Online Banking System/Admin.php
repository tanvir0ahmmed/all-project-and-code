<?php
include "conct.php"; 
?>
<html>
	<head>
	<title>Table V03</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/perfect-scrollbar/perfect-scrollbar.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="M_CSS/util.css">
	<link rel="stylesheet" type="text/css" href="M_CSS/main.css">
	<link rel="stylesheet" href="M_CSS/Admin.css">
<!--===============================================================================================-->
</head>
	<body>

  <div class="link" id = "menu">
  	
  </div>

  <div class="data">
  <div class="limiter">
		<div class="container-table100">
			<div class="wrap-table100">
				<div class="table100 ver1 m-b-110">
    <h2> Applied Client Information</h2>
    <table data-vertable="ver1" width="832" border="1" align="center" cellpadding="5">
      <thead>
							<tr class="row100 head">
								<th class="column100 column1" data-column="column1">ID</th>
								<th class="column100 column2" data-column="column2">Name</th>
								<th class="column100 column3" data-column="column3">NID</th>
								<th class="column100 column4" data-column="column4">Address</th>
								<th class="column100 column5" data-column="column5">Branch</th>
								<th class="column100 column6" data-column="column6">Action</th>
							</tr>
						</thead>
      <?php
        	//$sql = "select user_id,username, address, created_at, modified_at from tb_users";
      	$sql = "select * from applied";
      	
      	$result = mysqli_query($conn, $sql) or die("Error: ".mysqli_error($conn));
      	//$a=mysqli_fetch_array($result);
      	//echo $result["username"];
        $i=0;
      	if(mysqli_num_rows($result) > 0)
      		{
      		while($row = mysqli_fetch_assoc($result))	
      			{
              $i++;
        //echo $row["username"];
      ?>
      
      <tr>
     	<td><?php echo $i ?></td>
        <td><?php echo $row['username'] ?></td>
        <td><?php echo $row['nid'] ?></td>
        <td><?php echo $row['address'] ?></td>
        <td><?php echo $row['branch'] ?></td>
        <td>
        	<button class="button"><span><a href="update.php?id=<?php echo $row['id']; ?>">CONFIRM </span></button>
			<button class="button"><span><a href="add.php?id=<?php echo $row['id']; ?>">INSERT </span></button>
			<button class="button"><span><a href="delete.php?id=<?php echo $row['id']; ?>">DELETE </span></button>
           
        </td>
        
      </tr>
    <?php
    		//echo "<tr><td>".$row["id"]."</td><td>".$row["username"]."</td><td>".$row['address']."</td><td>".$row["email"]."</td><td>".$row['mobile']."</td></tr>";	
    			
    			}
    		}

    ?>
    </table>
	</div>
	</div>
	</div>
	</div>
  </div>
		<!--===============================================================================================-->	
	<script src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/bootstrap/js/popper.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>

	</body>
	
	
</html>