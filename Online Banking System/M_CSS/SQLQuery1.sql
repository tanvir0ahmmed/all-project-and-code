
use swarna
go
create table applied(
	id int,
	username varchar(150),
	nid varchar(150),
	address varchar(150),
	branch varchar(150),
	password varchar(150),
	CONSTRAINT PK_Customer_appl PRIMARY KEY (id)
);
create table accept_info(
	id int,
	username varchar(150),
	nid varchar(150),
	address varchar(150),
	branch varchar(150),
	password varchar(150),
	CONSTRAINT PK_Customer_acpt PRIMARY KEY (id)
);
create table policy(
	username varchar(150),
	deposit int,
	loan int,
	transfer int,
	CONSTRAINT PK_policy PRIMARY KEY (username)
);

create table employee(
	emp_id varchar(150),
	name varchar(150),
	branch varchar(150),
	password varchar(150),
	CONSTRAINT PK_emp_info PRIMARY KEY (emp_id)
);

create table transection(
	policy_user varchar(150),
	transection_no int,
	amount varchar(150),
	where_to varchar(150),
	CONSTRAINT PK_transection PRIMARY KEY (transection_no),
	FOREIGN KEY (policy_user) REFERENCES policy(username)
);

create table record(
	rec_user varchar(150),
	branch varchar(150),
	datee date,
	transection_no int,
	FOREIGN KEY (rec_user) REFERENCES policy(username),
	FOREIGN KEY (transection_no) REFERENCES transection(transection_no)
);
create table manager(
	emp_id varchar(150),
	username varchar(150),
	transection_no int,
	FOREIGN KEY (emp_id) REFERENCES employee(emp_id),
	FOREIGN KEY (transection_no) REFERENCES transection(transection_no),
	FOREIGN KEY (username) REFERENCES policy(username)
);
