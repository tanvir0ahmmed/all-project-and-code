-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 21, 2018 at 08:01 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bank`
--

-- --------------------------------------------------------

--
-- Table structure for table `accept_info`
--

CREATE TABLE `accept_info` (
  `id` int(11) NOT NULL,
  `username` varchar(150) DEFAULT NULL,
  `nid` varchar(150) DEFAULT NULL,
  `address` varchar(150) DEFAULT NULL,
  `branch` varchar(150) DEFAULT NULL,
  `password` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accept_info`
--

INSERT INTO `accept_info` (`id`, `username`, `nid`, `address`, `branch`, `password`) VALUES
(0, 'swarna', '1234553645', 'Tangaile', 'Dhaka', '12345'),
(2, 'amin', '334245', 'dhaka', 'Dhaka', '123');

-- --------------------------------------------------------

--
-- Table structure for table `applied`
--

CREATE TABLE `applied` (
  `id` int(11) NOT NULL,
  `username` varchar(250) NOT NULL,
  `nid` int(11) NOT NULL,
  `address` varchar(250) NOT NULL,
  `branch` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `applied`
--

INSERT INTO `applied` (`id`, `username`, `nid`, `address`, `branch`, `password`) VALUES
(2, 'amin', 334245, 'dhaka', 'Dhaka', '123');

-- --------------------------------------------------------

--
-- Table structure for table `deposite`
--

CREATE TABLE `deposite` (
  `id` int(11) NOT NULL,
  `username` varchar(150) NOT NULL,
  `deposite` int(150) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `emp_id` int(11) NOT NULL,
  `username` varchar(150) DEFAULT NULL,
  `branch` varchar(150) DEFAULT NULL,
  `password` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`emp_id`, `username`, `branch`, `password`) VALUES
(0, 'mahin', 'Ghataile', '1234'),
(1, 'alu', 'dhaka', 'alu'),
(2, 'swarna', 'Dhaka', '1234');

-- --------------------------------------------------------

--
-- Table structure for table `loan`
--

CREATE TABLE `loan` (
  `id` int(11) NOT NULL,
  `username` varchar(150) NOT NULL,
  `loan` int(150) NOT NULL,
  `about` varchar(150) NOT NULL,
  `date` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `manager`
--

CREATE TABLE `manager` (
  `emp_id` int(11) DEFAULT NULL,
  `username` varchar(150) DEFAULT NULL,
  `transection_no` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `policy`
--

CREATE TABLE `policy` (
  `username` varchar(150) NOT NULL,
  `deposit` int(11) DEFAULT NULL,
  `loan` int(11) DEFAULT NULL,
  `transfer` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `record`
--

CREATE TABLE `record` (
  `rec_user` varchar(150) DEFAULT NULL,
  `branch` varchar(150) DEFAULT NULL,
  `datee` date DEFAULT NULL,
  `transection_no` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transection`
--

CREATE TABLE `transection` (
  `policy_user` varchar(150) DEFAULT NULL,
  `transection_no` int(11) NOT NULL,
  `amount` varchar(150) DEFAULT NULL,
  `where_to` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accept_info`
--
ALTER TABLE `accept_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `applied`
--
ALTER TABLE `applied`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deposite`
--
ALTER TABLE `deposite`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`emp_id`);

--
-- Indexes for table `loan`
--
ALTER TABLE `loan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `manager`
--
ALTER TABLE `manager`
  ADD KEY `emp_id` (`emp_id`),
  ADD KEY `transection_no` (`transection_no`),
  ADD KEY `username` (`username`);

--
-- Indexes for table `policy`
--
ALTER TABLE `policy`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `record`
--
ALTER TABLE `record`
  ADD KEY `rec_user` (`rec_user`),
  ADD KEY `transection_no` (`transection_no`);

--
-- Indexes for table `transection`
--
ALTER TABLE `transection`
  ADD PRIMARY KEY (`transection_no`),
  ADD KEY `policy_user` (`policy_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `applied`
--
ALTER TABLE `applied`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `deposite`
--
ALTER TABLE `deposite`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `loan`
--
ALTER TABLE `loan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `manager`
--
ALTER TABLE `manager`
  ADD CONSTRAINT `manager_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `employee` (`emp_id`),
  ADD CONSTRAINT `manager_ibfk_2` FOREIGN KEY (`transection_no`) REFERENCES `transection` (`transection_no`),
  ADD CONSTRAINT `manager_ibfk_3` FOREIGN KEY (`username`) REFERENCES `policy` (`username`);

--
-- Constraints for table `record`
--
ALTER TABLE `record`
  ADD CONSTRAINT `record_ibfk_1` FOREIGN KEY (`rec_user`) REFERENCES `policy` (`username`),
  ADD CONSTRAINT `record_ibfk_2` FOREIGN KEY (`transection_no`) REFERENCES `transection` (`transection_no`);

--
-- Constraints for table `transection`
--
ALTER TABLE `transection`
  ADD CONSTRAINT `transection_ibfk_1` FOREIGN KEY (`policy_user`) REFERENCES `policy` (`username`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
