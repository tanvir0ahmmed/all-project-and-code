-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 23, 2018 at 06:18 PM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bank`
--

-- --------------------------------------------------------

--
-- Table structure for table `accept_info`
--

CREATE TABLE `accept_info` (
  `id` int(11) NOT NULL,
  `username` varchar(150) DEFAULT NULL,
  `nid` varchar(150) DEFAULT NULL,
  `address` varchar(150) DEFAULT NULL,
  `branch` varchar(150) DEFAULT NULL,
  `password` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accept_info`
--

INSERT INTO `accept_info` (`id`, `username`, `nid`, `address`, `branch`, `password`) VALUES
(0, 'swarna', '1234553645', 'Tangaile', 'Dhaka', '12345'),
(2, 'amin', '334245', 'dhaka', 'Dhaka', '123'),
(3, 'fatima', '34234124', 'dhaka', 'dhaka', '2323'),
(4, 'swarnav', '233451', 'Dhaka', 'Dhaka', '123'),
(5, 'Mahi', '23456', 'ghtl', 'ghtl', '6767');

-- --------------------------------------------------------

--
-- Table structure for table `applied`
--

CREATE TABLE `applied` (
  `id` int(11) NOT NULL,
  `username` varchar(250) NOT NULL,
  `nid` int(11) NOT NULL,
  `address` varchar(250) NOT NULL,
  `branch` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `applied`
--

INSERT INTO `applied` (`id`, `username`, `nid`, `address`, `branch`, `password`) VALUES
(3, 'fatima', 34234124, 'dhaka', 'dhaka', '2323'),
(4, 'swarnav', 233451, 'Dhaka', 'Dhaka', '123');

-- --------------------------------------------------------

--
-- Table structure for table `deposite`
--

CREATE TABLE `deposite` (
  `id` int(11) NOT NULL,
  `username` varchar(150) NOT NULL,
  `deposite` int(150) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `deposite`
--

INSERT INTO `deposite` (`id`, `username`, `deposite`, `date`) VALUES
(1, 'swarna', 100000, '0000-00-00'),
(2, 'swarna', 100000, '2018-12-12');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `emp_id` int(11) NOT NULL,
  `username` varchar(150) DEFAULT NULL,
  `branch` varchar(150) DEFAULT NULL,
  `password` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`emp_id`, `username`, `branch`, `password`) VALUES
(0, 'mahin', 'Ghataile', '1234'),
(1, 'alu', 'dhaka', 'alu'),
(2, 'swarna', 'Dhaka', '1234');

-- --------------------------------------------------------

--
-- Table structure for table `loan`
--

CREATE TABLE `loan` (
  `id` int(11) NOT NULL,
  `username` varchar(150) NOT NULL,
  `loan` int(150) NOT NULL,
  `about` varchar(150) NOT NULL,
  `date` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `loan`
--

INSERT INTO `loan` (`id`, `username`, `loan`, `about`, `date`) VALUES
(1, 'swarna', 300000, 'Car Loan', 12),
(2, 'swarna', 500000, 'Home Loan', 2018);

-- --------------------------------------------------------

--
-- Table structure for table `manager`
--

CREATE TABLE `manager` (
  `emp_id` int(11) DEFAULT NULL,
  `username` varchar(150) DEFAULT NULL,
  `transection_no` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `policy`
--

CREATE TABLE `policy` (
  `username` varchar(150) NOT NULL,
  `deposit` int(11) DEFAULT NULL,
  `loan` int(11) DEFAULT NULL,
  `transfer` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `record`
--

CREATE TABLE `record` (
  `rec_user` varchar(150) DEFAULT NULL,
  `branch` varchar(150) DEFAULT NULL,
  `datee` date DEFAULT NULL,
  `transection_no` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transection`
--

CREATE TABLE `transection` (
  `policy_user` varchar(150) DEFAULT NULL,
  `transection_no` int(11) NOT NULL,
  `amount` varchar(150) DEFAULT NULL,
  `where_to` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wow`
--

CREATE TABLE `wow` (
  `Photo` blob NOT NULL,
  `Name` varchar(150) NOT NULL,
  `Father` varchar(150) NOT NULL,
  `Mother` varchar(150) NOT NULL,
  `username` varchar(150) NOT NULL,
  `password` varchar(150) NOT NULL,
  `Home` varchar(150) NOT NULL,
  `Birth` date NOT NULL,
  `Married` varchar(150) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wow`
--

INSERT INTO `wow` (`Photo`, `Name`, `Father`, `Mother`, `username`, `password`, `Home`, `Birth`, `Married`, `id`) VALUES
(0xffd8ffe000104a46494600010101006000600000ffdb0043000302020302020303030304030304050805050404050a070706080c0a0c0c0b0a0b0b0d0e12100d0e110e0b0b1016101113141515150c0f171816141812141514ffdb00430103040405040509050509140d0b0d1414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414141414ffc0001108007b006403012200021101031101ffc4001f0000010501010101010100000000000000000102030405060708090a0bffc400b5100002010303020403050504040000017d01020300041105122131410613516107227114328191a1082342b1c11552d1f02433627282090a161718191a25262728292a3435363738393a434445464748494a535455565758595a636465666768696a737475767778797a838485868788898a92939495969798999aa2a3a4a5a6a7a8a9aab2b3b4b5b6b7b8b9bac2c3c4c5c6c7c8c9cad2d3d4d5d6d7d8d9dae1e2e3e4e5e6e7e8e9eaf1f2f3f4f5f6f7f8f9faffc4001f0100030101010101010101010000000000000102030405060708090a0bffc400b51100020102040403040705040400010277000102031104052131061241510761711322328108144291a1b1c109233352f0156272d10a162434e125f11718191a262728292a35363738393a434445464748494a535455565758595a636465666768696a737475767778797a82838485868788898a92939495969798999aa2a3a4a5a6a7a8a9aab2b3b4b5b6b7b8b9bac2c3c4c5c6c7c8c9cad2d3d4d5d6d7d8d9dae2e3e4e5e6e7e8e9eaf2f3f4f5f6f7f8f9faffda000c03010002110311003f00fd3e5638eb4c690d099da29ad41080b9f5a89e4a18d4323055249e05268d056907702abcf716ea024c6201ced0b26393e9cf5afcd6fdb0bfe0a7d3681ab6a7e0ff0085e511eda46b7b8f11361cb3a921840bd319fe33d7b01c13f9c3e2df8c3e2ef1c6b126a5ad6bfa86a376cfe67997372ee437a8c9e3f0a686d58fe9123b3b48999a3b685188c12a8066964b7825528f0c6e8dc15640457f3b7e0ffda7be26f81a6dfa378e75ed3c7431c37f2846faaeec1fcabec3fd9e7fe0ac3e29d0af2d34cf8956b1f89b49e11f52b68d62be8c7f7881849001d8804f76ad1224fd5e3a7d9974636d0931fdc3e58f97e9c7149f62b7123c8208bcc90619b60cb0f73deb27c0de39d07e24f85ec7c45e1ad4e0d5f47bd4df0dd5bb641f50475560782a70411835bfb68b0ec4515ac023f2fc88fcbe9b768c7e5f8d4ffd9f6db7e4b7897fdd40285183915663e4504b30ee34c83cc398109ff74515a7347ba4cd141162fa9fe548c05387dd149528b440cb5f3c7edddf1ce7f803fb39f8835db042757bf2349b06c95f2e6995879990739545761ee057d14d5f09ff00c1612376fd97f496085827892d8939236fee2e067f5a4cd628fc65bbba92f2779646dccc726a38e379182a2963e805743e03f04df78f7c410e996319666e5d80fbabeb5f7efc23fd927c31a1e851497910b8d41802ef27cc01f6cd79d89c6c30ef96d79763ddcbb26ab8f8fb66f961b5fbfa23f38a5b79206c488c87d186292395a339538afd58f107ecb7e04f1369e52f749b7f3b61413a205703d88e95f057ed09fb3edd7c20d61a5b577bad1a57223948e53d9a950c742ab516acd9ae3b23ab8584aad3973c56fd1a3e90ff008254fed017be11f8d0be05bfd418683e2489d23b7958f96978abba375ec198294f7dcbe82bf62f757f3d5fb17eb16fa1fed49f0d2e6e9b6c1fdb96b1938ce0b481476f5615fd0a274af596a8f9c4b41fed53455147f330a9570ac47a53131b24659b838a2ac0a2a481ca7e514b5129f9452eea438815af8bbfe0ac1259cffb2bddd8c8ea7507d46dee6da1da4b308db1230c74c2bf24fafbd7d9fbabc3ff006bcf04d978cfe0fdf7daace3bd6b475754923de36b7c8c08ee30d9fc05615e6e9d3735d0f4f054615ebc29547652d3e6f6fc4fca3f859e1b8fe13f81741f11dbe8eda94faa5badcde5c79cb198d4f2a06ec0c608afa03e12fed2be1ef196ad6fa2ff0067dd58dd39da1fcc4923ce3d41adad17e12e97e22f09f876cb623ae9f6f6ff0067df18650c8abb4943c1e474349a5fc0ad1bc0564b1dad940b31bf92fbcf556de1dc0046e6662146d185040e2be42a4e13e6a935ef1faa61a94a9c69d18bb452d576f4d3efd4f41f1e78d34bf00e92b7da825c4f0b1dbfe8ea188e33d335f3978dbc4fa7fc6ed3e5d2edfc35ad3e9b77f23de4d6ea82127a3825b9c7f915ef9e2af0adb78c7ecb6f7d047771aa8c432e4a311fde008c8ae1bc0ffb3369ff000f6dac9eddd8496a656370a48927dec0e242301828185e38e79e6aa1c8e2e5d56c6f38ca2d2be8ef7ff863e2ef833f0bae3c3ffb63f82bc28d37982cfc4762cf71b768f284b1c9b8fa7ca40fa9afe8097eed7e77fc27f831a4788be3ededf0b343a8de3c2af75b8ee8e18a346718e809c0e7d857e89015f5585ad2ab0d56df9f53f30ccb030c14d28bf8aeedd95f4157e56069c1b2c79a6ad2afdeaede87865b8f3b68a48c7cbffd7a2a08231f7451f8549b7e5146de28122139aa7ab69906b5a75d58dd27996f711b4522fb11835a0568dbed49a4f43a21269dd1f117883435f867e3ad434357f361b770d1395db95601871f8e3f0accf1b6b0f22dab5b59bdf0dc3747148aa7ebc9e7e82bd37f6c0f0e9d1f50d1fc5311511cf8b19d7a1dc32c87df2370ff00808af9a352b7d6b50d4a19ecb58b8b6b7c0cc30431b9c8e87e6073f4c8af89c5c1d2ace9ec8fd6f29a9f5aa30a8debd7d4f558f51171a8d949069f750a14e5e5daa148eb90486fc85696b9a962cca28e7dabcdac6c7c4b75710ccdacb08c60c91dcd92a647b6d6ebef9ad7d63c61a3f87fca9fc41a90d3b4985d4ddde3233f951e4066c0049c0f415ac1734941753ab12d524db77b1f4bfc03f8571f866cff00e122b86592f7528c4aab8ff56ac17a9f5daaa3ff00d75ecc071d2aae93f656d32d0d8b23d99853c868ce54a6d1b483e98c55d506bec69d354e3ca8fc8b11889e22a3a936340f5a555cb53b6d39473d2b438996621f2fff005e8a96153e5d150491aa6e5a369e94f5e82ab6a9aad9e916e66bcb98ede35eee719fa7ad5a44264acb8aa7a9ea965a2da49757f790595b46a59e5b89022a81d4924d79778bbe3e5b5bb496fa341e6ba839b89ba0c7a2f7fc7f2af07f8817779e3c664d4af669bccfe1dc70abc062074ce0f07b122bba960a75359e888962231765a9c6fedfdf1327d4bc55f0f974abe33f85a46b9b796485bf76f3baa346defc23807dcfad78a3fc51d4fc056a93368d36ac8a328d0f51f515eb1f1bb49d33c61e09bbf0ac5756f1ebcb00bdd36c84a8b26e8595832a75c03b57238c31accf03d8c7a9e8565f6ab4473b06e5917a1ee2be433da2b0f569ced74d7e28fd3f86714ab60a749ef17f83fe9993e11f8e179e39b1db1681736b211feba65da83f3e4fe15c57ed116b2c9f0c75c599da4b8b987cb55fef3310001f8915f43db787ac6ce102d2ca1b7ff74579b7c56f0fa6a1ae784ec2465114da8adc4dbba048819003f5655af2b0b1757134e31d2ed7e67bb89a90a787a8edb45fe47d87fb3878b85bfc3ef0fe8daa4fb6686c2058e498fa4632a4fae6bdb948ea3915f2b69f318ede00870a1147cbe98f7ae8fc2bf11357d01da0171f6a86338f2a6c9e3a6457ea588c1f349ce07e1b4f12b691f4405a51d6b8cf0cfc4ed3b5ec24bfe8b3676fcc7e5cff004fe5ef5d9a10d820e41e84578d384a9bb491d4a4a5aa2f5bff00abf4a292151b28ae71dce4be2478d17c07e11b8d59821319545321f941638c9f615f395d78c350f16df3dd5fdc7da61ce5486f94a919c0ed8fa76fad7a7fed652b43f03f586504bef895768c9c9603fad7cc3f02e4371f0af4979242483329ce791e7b81fa0fd2be9303462e97b5b6b76bf2ff0033cdad27cb7b9db6a7a9dbe9f6d70ed11924dadb2340092d9271efd2bce3c55e22f11de5d5ce99e14d2cadfe235b9d5af17fd1a027f8235fe361939f4ef9e2bd22da356f34b10c4e4671d0fb7f9ef576086dd2375dbe5a8e4201fc5d4ff4af522e3195dab9c2e5d8f833c41fb34fc4af09788a3f892be20975ff00145a49f6925242edb4632983d41567053a6011debea3f851e27d1fc5fa5cb7d6ede4ced21f3ec64e1eddc9e548feee7a1fc3a835e9ad0aac615c2146e4e3a12723f962bce75af857651ea6fa9698eda6df9f9e2b841f286007caca0fcc8c14023f1183cd78b9865b4f30a6e2b496ebd4fa4caf38781aaa525eebdceed60821c90303fda35e6df1426d3b439adf5a96633dcc11b3c56bbc0321e8a8a3dcf56e838cf7abb7dad6a3ab5a9b2b7877dfc630d1c672aee73b46ef4e327be31c64815a7e0ff83f656e8973ab7fc4cb579e5f3e69e5192ae7b0e78c0e07a7e95e464593aa53789c6ab72bb25d5b5d7d3ccfa2cf33c84e9aa1859df996afc9f4f9f5392f837fb44df6b5a1daff00c2c5d125f09ea724a615d43ca6feceb939014893911939c618f27a13d07b7cd65f6e9ade4128f2241925186d72318c7af152dc78574db8d105979119b4c6df2f036903b7d2b96d37e1f43e1f62fa35fdd69d618e74fde248236073f2020b2741c290bed5f62f93a1f9df35ddcee96dc436aab10d9839caf5cd779f0c7c797936ad6da35c93224aa7617ea3009e3f2e95c4daa936a379cba81573c0ede4fc4ef0fc71b906633b30edb563231f9b0fcab82b414e124d6c9b3784acd58fa4edf3e5d14eb7c797f8d15f2c7a973c47f6b1ba36bf04f5360db0b5c5ba06fee932a8073db071cf6af95fe115e2b7c3bd39924c67ce949f2fef1f39d88c741cfa71d857d31fb6a796bfb3bf88249e3792de3781a5f2cfccabe62e587d3afe15f22fc09d4a49be17f871d1cf3148aee41203091c13f5c8afabcb65fb851f37fa1e462bf8575dff00ccf4f8ef040ccf186731b153e818919247d4fe95b10c8248f3d485ce7047381cfe42b33eca66b125c9566e49c7b823f0c8f5a5d2adcb339966c84c679e3807fc3f9d76cbb9e7f374365f6aa9f977fcc769c76cd63eb57f269fa7cee8be634685d53a678240cfd47eb5a30a3b796a498fe51bb279c82327f31fad4ed099994850f229ee320e0e4fe99a956dc14ba16b4df0de8de1ed2ece1b4b6275110c86eae9ce5a590e58b0cf4fc3b6076ab76aa51431241c923273d8d429783cb76e7608c7ea40c7eb514178b25b89167491776df9483d0f3fc8d4ddf52f9ae6c432192d5633f74f39c74f7ace9a4453242e0348abbb6fa7f9e6addbdc0283cb911caa83b7eb9aaf7bb9a78d97e5e80f7a9bea3e62b68da937daaf2ca4769248646dbb8f250f23f5c8ae8be1e49e77c5cd1e32d8582de460a0f5660f9fd17f5af3fd7b515d07c4d6b76edb20b81e5bb7400e7009fa714dfd9f3c7f178cfe3b34965289f4f4678219147df5489b2c3d8b1ebf4ad2a536e9549c56966553937247db76ff00eaff001a29b0b0f2fa515f127b5cc707f1a7c1179f10be1ddfe85622069ee8a2b2dc9c215ddf303c1ed9af997e0cfec83e3cf87fe0d6d1afee34d77b7ba9bece63b92dba16725493b38386e47a8afb5158e282e7d6bb68e2aa515cb038a50535667cd4dfb3ef8b8c8aad2d9794a980bf6838dd9ce7eed4d6bf007c4b0c787fb0bca0e448676c0fc36f2718eb5f461639eb4df31b1d6b596615bc8ce38781f3effc289f140cb19ec8e09c7ef4f4ff00be69d67f047c4f6ca4b4d64edf746e99b1824e4fddeb835efe5cfad4458fad4acc2b5ba15f56a77b9e0f77f057c4bf619a2827b1f3245c06791b83953fddf635cb7c29fd99bc47e0df00e97a1dfdc588b9b6de649219de4562ce589c951eb9fc6be9c2c7763b522b1c1a9faf55bdcd1518a8b8747fa5ff00ccf06b8f80de21dade45cd831278df238e3f05a9a1f825e275f2c7daec00523fe5ab9e33ee9e95ee818e4f34864619e6abfb42b7919fd5e99f3f78e3f676d7fc4b630436d7ba7c6eb2ee7691dc7cbc74214f3c562fece3fb2c788be0ef886d6ff53bed2e648e494c9f6396466652aca83e641d0119fc7dabe9592e240d80dc7d0564dc6a9751c8c165c0e3f847a7d2ade678874a54afa31aa108ea8eaa3fbb45713ff0906a0bc0b8f5fe05f5fa515e45ce93ffd9, 'Jannatul Ferdous Swarna', 'S.M.A Salam', 'Aklima', 'swarna', 'swarna', 'Tangail', '1996-11-04', 'No', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accept_info`
--
ALTER TABLE `accept_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `applied`
--
ALTER TABLE `applied`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deposite`
--
ALTER TABLE `deposite`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`emp_id`);

--
-- Indexes for table `loan`
--
ALTER TABLE `loan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `manager`
--
ALTER TABLE `manager`
  ADD KEY `emp_id` (`emp_id`),
  ADD KEY `transection_no` (`transection_no`),
  ADD KEY `username` (`username`);

--
-- Indexes for table `policy`
--
ALTER TABLE `policy`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `record`
--
ALTER TABLE `record`
  ADD KEY `rec_user` (`rec_user`),
  ADD KEY `transection_no` (`transection_no`);

--
-- Indexes for table `transection`
--
ALTER TABLE `transection`
  ADD PRIMARY KEY (`transection_no`),
  ADD KEY `policy_user` (`policy_user`);

--
-- Indexes for table `wow`
--
ALTER TABLE `wow`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `applied`
--
ALTER TABLE `applied`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `deposite`
--
ALTER TABLE `deposite`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `loan`
--
ALTER TABLE `loan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wow`
--
ALTER TABLE `wow`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `manager`
--
ALTER TABLE `manager`
  ADD CONSTRAINT `manager_ibfk_1` FOREIGN KEY (`emp_id`) REFERENCES `employee` (`emp_id`),
  ADD CONSTRAINT `manager_ibfk_2` FOREIGN KEY (`transection_no`) REFERENCES `transection` (`transection_no`),
  ADD CONSTRAINT `manager_ibfk_3` FOREIGN KEY (`username`) REFERENCES `policy` (`username`);

--
-- Constraints for table `record`
--
ALTER TABLE `record`
  ADD CONSTRAINT `record_ibfk_1` FOREIGN KEY (`rec_user`) REFERENCES `policy` (`username`),
  ADD CONSTRAINT `record_ibfk_2` FOREIGN KEY (`transection_no`) REFERENCES `transection` (`transection_no`);

--
-- Constraints for table `transection`
--
ALTER TABLE `transection`
  ADD CONSTRAINT `transection_ibfk_1` FOREIGN KEY (`policy_user`) REFERENCES `policy` (`username`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
